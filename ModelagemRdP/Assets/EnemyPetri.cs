using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RdPengine;

public class EnemyPetri : MonoBehaviour
{
    public float speed;

    public PetriNet boss;
    private Place value;

    [SerializeField]
    private Rigidbody2D rb2d;

    private float horMov = 3.0F;
    private float verMov = 3.0F;
    private float fator = 1.0F;
    // Start is called before the first frame update
    void Awake()
    {
        boss = new PetriNet("Assets/inimigo.pflow");
        value = boss.GetPlaceByLabel("value");
        rb2d = GetComponent<Rigidbody2D>();
    }
    private void FixedUpdate()
    {
        //Debug.Log($"Horizontal - {horMov}; Vertical - {verMov}");
        //Vector2 movement = new Vector2(horMov, verMov);
        //rb2d.AddForce(movement * speed);
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (boss == null) return;
        boss.GetPlaceByLabel("Away").Tokens = 1;
        switch (value.Tokens)
        {
            case 1:
                horMov = -fator;
                break;
            case 2:
                horMov = fator;
                break;
            case 3:
                verMov = fator;
                break;
            case 4:
                verMov = -fator;
                break;
        }
    }
}