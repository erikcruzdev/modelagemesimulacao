using Priority_Queue;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dijkstra : MonoBehaviour
{
    private readonly SimplePriorityQueue<Tile, double> _frontier = new SimplePriorityQueue<Tile, double>();
    private readonly HashSet<Tile> _visitedTiles = new HashSet<Tile>();
    private readonly Dictionary<Tile, double> _frontierCost = new Dictionary<Tile, double>();
    private readonly Dictionary<Tile, Tile> _frontierParent = new Dictionary<Tile, Tile>();

    public static Dijkstra Instance;
    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }

    public void ResetStructures() {
        _frontierParent.Clear();
        _visitedTiles.Clear();
    }

    private void AddToFrontier(Tile tile, double cost, Tile parent)
    {
        if (!_frontierCost.TryGetValue(tile, out var existingCost))
        {
            _frontierCost.Add(tile, cost);
            _frontierParent.Add(tile, parent);
        }
        else
        {
            if (cost < existingCost)
            {
                _frontierCost[tile] = cost;
                _frontierParent[tile] = parent;
            }
        }
        _frontier.Enqueue(tile, cost);
    }


    public bool FindPath(Tile enemyStartPos, Tile destination, List<Tile> outResult){
        _frontier.Clear();
        _frontierCost.Clear();
        _frontierParent.Clear();
        _visitedTiles.Clear();
        AddToFrontier(enemyStartPos, 0, null);
        bool foundPath = false;
        while (_frontier.Count > 0)
        {
            var tile = _frontier.Dequeue();
            double tileCost = _frontierCost[tile];

            if (_visitedTiles.Contains(tile)) continue;

            _visitedTiles.Add(tile);

            if (tile == destination) {
                foundPath = true;
                break;
            }

            for (int dx = -1; dx <= 1; dx++)
            {
                for (int dy = -1; dy <= 1; dy++)
                {
                    if(dx == 0 && dy == 0) continue;

                    Vector2Int neighborPosition = tile.Position;
                    neighborPosition.x += dx;
                    neighborPosition.y += dy;
                    Tile neighborSpot = MapGenerator.Instance.Tiles[neighborPosition.x, neighborPosition.y];

                    if (neighborSpot == null || _visitedTiles.Contains(neighborSpot) || !neighborSpot.IsWalkable)
                    {
                        continue;
                    }

                    AddToFrontier(neighborSpot, tileCost + neighborSpot.PathCost, tile);
                }
            }
        }

        if (foundPath)
        {
            outResult.Clear();

            Tile tile = destination;
            while (tile != null)
            {
                outResult.Add(tile);
                tile = _frontierParent[tile];
            }

            outResult.Reverse();
        }

        return foundPath;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
