using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct cell
{
    public int bin;
    public int x;
    public int y;
    public Tile gObj;
    public double cost;
    public const double PositiveInfinity = double.PositiveInfinity;

    public void SetCost(double newCost)
    {
        if (this.bin == 0)
        {
            this.cost = PositiveInfinity;
        }
        else
        {
            this.cost = newCost;
        }
    }  
    public cell(int x, int y, int bin, double newCost)
    {
        this.x = x;
        this.y = y;
        this.bin = bin;
        this.gObj = null;
        if (this.bin == 0)
        {
            this.cost = PositiveInfinity;
        }
        else
        {
            this.cost = newCost;
        }
    }
    public cell(int x, int y, int bin, Tile gO, double newCost)
    {
        this.x = x;
        this.y = y;
        this.bin = bin;
        this.gObj = gO;
        if (this.bin == 0)
        {
            this.cost = PositiveInfinity;
        }
        else
        {

            this.cost = newCost;
        }
    }
    
}

public class FloodFill : MonoBehaviour
{
    public const double PositiveInfinity = double.PositiveInfinity;
    public cell[,] matriz;
    [SerializeField] private int interationsMax = 255;
    private int InterationsCounter = 0;

    [SerializeField] private int randomMax = 255;

    private int width;
    private int height;
    public GameObject player;
    public GameObject chave;
    public GameObject portal;
    public GameObject enemy;

    private DijkstraEnemy _enemyFollower;
    private Tile _playerPos;

    public Color _tileColor;

    private List<Tile> _keysAndGateToPatrol = new List<Tile>();

    //Parametriza��o
    [SerializeField]
    private int _numberOfKeys = 5;

    public int NumberOfKeys => _numberOfKeys;

    void Start()
    {
        
    }

    public void StartMatrix(int width, int height) {
        this.width = width;
        this.height = height;
        matriz = new cell[width, height];
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //1 = pintado
    //0 = n�o pintado
    public void MakeFloodFill(int gridX, int gridY) {
        bool doing = true;
        Stack celulas = new Stack();
        cell atual;
        int jogadoresnaocolocados = 1;
        int chavesnaocolocadas = 5;
        int portlanaocolocado = 1;
        int inimigonaocolocado = 1;

        //Parametriza��o
        chavesnaocolocadas = _numberOfKeys;

        if (gridX >= 0 && gridX < width && gridY >= 0 && gridY < height)
        {
            if (matriz[gridX, gridY].bin == 0)
            {
                celulas.Push(matriz[gridX, gridY]);
            }
            else
            {
                doing = false;
            }
            while (doing) {
                InterationsCounter++;
                if (celulas.Count == 0)
                {
                    doing = false;
                }
                else {
                    atual = (cell)celulas.Pop();
                    gridX = atual.x;
                    gridY = atual.y;
                    
                    if (matriz[gridX, gridY].bin == 0) {//atual
                        matriz[gridX, gridY].bin = 1;
                        double cost = Random.Range(0, 99);
                        matriz[gridX, gridY].cost = cost;

                        matriz[gridX, gridY].gObj.SetCost(cost);
                       // Debug.Log($"Cell cost at {gridX} , {gridY} is = {cost}");
                        Color newColor = new Color(1, 1, 1, 1);
                        newColor.r = 1.0f - Mathf.Clamp01((float)(cost)/100);
                        newColor.g = 1.0f - Mathf.Clamp01((float)(cost) / 100);
                        newColor.b = 1.0f - Mathf.Clamp01((float)(cost) / 100);

                        newColor = _tileColor * newColor;
                        matriz[gridX, gridY].gObj.SetColor(newColor);


                        //int somatorionaocolocados = jogadoresnaocolocados + chavesnaocolocadas + portlanaocolocado + inimigonaocolocado;
                        if (InterationsCounter >= interationsMax) {
                            bool colocou = false;
                            int r = Random.Range(0, randomMax);
                            Debug.Log($"Counter is {InterationsCounter} random is {r} ");
                            InterationsCounter = 0;
                            if (jogadoresnaocolocados > 0) {
                                
                                if (r == 1) {
                                    colocou = true;
                                    Instantiate(player, matriz[gridX, gridY].gObj.transform.position, Quaternion.identity);
                                    jogadoresnaocolocados--;
                                    Debug.Log($"Spawnou player na {gridX} , {gridY} = random is {r}");
                                    _playerPos = matriz[gridX, gridY].gObj;
                                }
                            }
                            if (!colocou)
                            {
                                if (chavesnaocolocadas > 0) {                                  
                                    if (r == 2) {
                                        colocou = true;
                                        Instantiate(chave, matriz[gridX, gridY].gObj.transform.position, Quaternion.identity);
                                        chavesnaocolocadas--;
                                        _keysAndGateToPatrol.Add(matriz[gridX, gridY].gObj);
                                        Debug.Log($"Spawnou chave na {gridX} , {gridY} = random is {r}");
                                        //InterationsCounter = 0;
                                    }
                                }
                                if (portlanaocolocado > 0)
                                {                                       
                                    if (r == 3)
                                    {
                                        colocou = true;
                                        Instantiate(portal, matriz[gridX, gridY].gObj.transform.position, Quaternion.identity);
                                        _keysAndGateToPatrol.Add(matriz[gridX, gridY].gObj);
                                        portlanaocolocado--;
                                        Debug.Log($"Spawnou portal na {gridX} , {gridY} = random is {r}");
                                       //InterationsCounter = 0;
                                    }
                                }
                                if (inimigonaocolocado > 0)
                                {
                                     if (r == 4)
                                     {
                                         colocou = true;
                                         GameObject enemyGO = Instantiate(enemy, matriz[gridX, gridY].gObj.transform.position, Quaternion.identity);
                                         inimigonaocolocado--;
                                        Debug.Log($"Spawnou inimigo na {gridX} , {gridY} = random is {r}");
                                        _enemyFollower = enemyGO.GetComponent<DijkstraEnemy>();
                                        _enemyFollower.InitialPosition = matriz[gridX, gridY].gObj;
                                        
                                        //InterationsCounter = 0;

                                    }
                                }                                
                            }
                        }
                    }
                    if (gridX > 0 && gridY > 0) { //superior esquerdo
                        if (matriz[gridX - 1, gridY - 1].bin == 0)
                        {
                            celulas.Push(new cell(gridX - 1, gridY - 1, 0 , Random.Range(0,99)));
                           
                        }

                    }

                    if (gridY > 0)
                    { //superior
                        if (matriz[gridX, gridY - 1].bin == 0)
                        {
                            celulas.Push(new cell(gridX, gridY - 1, 0, Random.Range(0, 99)));
                        }

                    }

                    if (gridX < width - 1 && gridY > 0)
                    { //superior direito

                        if (matriz[gridX + 1, gridY - 1].bin == 0)
                        {
                            celulas.Push(new cell(gridX + 1, gridY - 1, 0, Random.Range(0, 99)));
                        }
                    }

                    if (gridX > 0)
                    { //esquerdo

                        if (matriz[gridX - 1, gridY].bin == 0)
                        {
                            celulas.Push(new cell(gridX - 1, gridY, 0, Random.Range(0, 99)));
                        }
                    }

                    if (gridX < width - 1) { //direito
                        if (matriz[gridX + 1, gridY].bin == 0) {
                            celulas.Push(new cell(gridX + 1, gridY, 0, Random.Range(0, 99)));
                        }
                    }

                    if (gridX > 0 && gridY < height - 1) { //inferior esquerdo
                        if (matriz[gridX - 1, gridY + 1].bin == 0)
                        {
                            celulas.Push(new cell(gridX - 1, gridY + 1, 0, Random.Range(0, 99)));
                        }

                    }

                    if (gridY < height - 1) { //inferior
                        if (matriz[gridX, gridY + 1].bin == 0)
                        {
                            celulas.Push(new cell(gridX, gridY + 1, 0, Random.Range(0, 99)));
                        }
                    }

                    if (gridX < width - 1 && gridY < height - 1) { //inferior direito
                        if (matriz[gridX + 1, gridY + 1].bin == 0)
                        {
                            celulas.Push(new cell(gridX + 1, gridY + 1, 0, Random.Range(0, 99)));
                        }

                    }

                }
            
            }
        }

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
               // Debug.Log($"Cost is at {x}, {y} = {matriz[x,y].cost} ");
            }
        }
        //SETA A POSI��O INICIAL DO INIMIGOOOOO!!!!!!!!!!!!
        _enemyFollower.MyTarget = _playerPos;
        _enemyFollower.FillPatrolList(_keysAndGateToPatrol);
     }

}
