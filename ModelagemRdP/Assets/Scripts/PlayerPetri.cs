using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RdPengine;

public class PlayerPetri : MonoBehaviour
{
    public static PlayerPetri Instance;
    public const string FILE_NAME = "Assets/jogador.pflow";
    public const string KEY_INCREASE = "Aumenta chaves";

    public const string VICTORY = "Victory";
    public const string GAME_OVER = "GameOver";
    public const string ENEMY_TAG = "Enemy";
    public const string KEY_TAG = "keys";
    public const string DOOR_TAG = "door";

    public const string COLLISION_ENEMY = "#EnemyCollision";
    public const string COLLISION_KEY = "#@Key";
    public const string COLLISION_DOOR = "#@Porta";

    //Tag para identificar as STONES
    public const string STONE_TAG = "Stone";
    //Refer�ncia das Stones
    private Tile _stone;
    //Getter Stone
    public Tile Stone => _stone;
    //Var's para guardar as posi��es X e Y do Tile onde o Player est� pisando.
    public int tilePosX;
    public int tilePosY;

    public PetriNet player;
    // Start is called before the first frame update
    private Place _keys;
    public Place KeysPlace => _keys;

    private Keys _keyInfo;

    //Parametriza��o
    private FloodFill _floodFill;

    private void Awake()
    {
        Instance = this;
    }
    void Start()
    {
        player = new PetriNet(FILE_NAME);
        _keys = player.GetPlaceByLabel(KEY_INCREASE);

        _keyInfo = GetComponent<Keys>();
        _keyInfo.SetPlayerReference(this);

        _keys.AddCallback(_keyInfo.RefreshUI, "refreshPontos", Tokens.In);

        player.GetPlaceByLabel(VICTORY).AddCallback(_keyInfo.RefreshWinCondition, "a", Tokens.In);
        player.GetPlaceByLabel(GAME_OVER).AddCallback(_keyInfo.RefreshWinCondition, "b", Tokens.In);
        player.GetPlaceByLabel(COLLISION_ENEMY).AddCallback(_keyInfo.RefreshWinCondition, "b", Tokens.In);
        _keyInfo.RefreshUI();


        //Parametriza��o (tem que ajeitar esse .Find)
        _floodFill = GameObject.Find("MapGenerator").GetComponent<FloodFill>();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag(ENEMY_TAG))
        {
            Debug.Log("<><><>- COLLISION WITH ENEMY");
            player.GetPlaceByLabel(COLLISION_ENEMY).Tokens = 1;
            _keyInfo.SetTextToUI("Game Over!!!");
            gameObject.SetActive(false);
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(KEY_TAG)) {
            player.GetPlaceByLabel(COLLISION_KEY).Tokens = 1;
        }
        if (other.gameObject.CompareTag(DOOR_TAG) && KeysPlace.Tokens== _floodFill.NumberOfKeys) {
            player.GetPlaceByLabel(COLLISION_DOOR).Tokens = 1;
            gameObject.SetActive(false);
        }
        //Caso o player pise num Tile com a tag STONE
        if (other.gameObject.CompareTag(STONE_TAG))
        {
            _stone = other.GetComponent<Tile>();
            //Guarda essa posi��o para emitir externamente
            tilePosX = _stone.PosX;
            tilePosY = _stone.PosY;
            //Debug.Log("Oi, sou uma linda princesa e estou na posi��o: " + tilePosX + "/ " + tilePosY);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
