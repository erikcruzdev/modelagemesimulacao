using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public static MapGenerator Instance { get; private set; }

    public Tile stone;

    public int width;
    public int height;

    public string seed;
    public bool useRandomSeed;

    [Range(0,100)]
    public int randomFillPercent;

    int[,] map;

    cell[,] cells;

    public FloodFill Floodfill;

    //public List<Tile> Stones = new List<Tile>();

    public Tile[,] Tiles;

   // public Tile[,] Tiles => _tiles;

    [SerializeField]
    private float yOffset;

    [SerializeField]
    private float xOffset;


    void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        GenerateMap();
       
        var middleX = width / 2;
        var middleY = height / 2;
        DesenharRochas();
        Floodfill.MakeFloodFill(middleX, middleY);

    }

    private void Update()
    {
        //if (Input.GetMouseButtonDown(0)) {
        //    for (int i = 0; i < Stones.Count; i++)
        //    {
        //        var stone = Stones[i];
        //        Destroy(stone);
        //    }
        //    Stones.Clear();
        //    GenerateMap();
        //    DesenharRochas();
        //}
    }

    void GenerateMap() {
        map = new int[width, height];
        RandomFillMap();
        for (int i = 0; i < 5; i++)
        {
            SmoothMap();
        }
    }

    void RandomFillMap() {
        if (useRandomSeed) {
            seed = Time.time.ToString();
        }
        System.Random pseudoRandom = new System.Random(seed.GetHashCode());
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                {
                    map[x, y] = 1;
                    
                }
                else
                {
                    map[x, y] = pseudoRandom.Next(0, 100) < randomFillPercent ? 1 : 0;
                }
            }
        }
    }

    void SmoothMap() {

        Floodfill.StartMatrix(width, height);

        cells = new cell[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int neighbourWallTiles = GetSurrondingWallCount(x, y);
                int curValue = -1;
                if (neighbourWallTiles > 4) {
                    curValue = 1;
                    map[x, y] = curValue;
                }
                else if (neighbourWallTiles < 4) {
                    curValue = 0;
                    map[x, y] = curValue;
                }
                cell curCell = new cell(x, y, curValue, Random.Range(0,99));
                cells[x, y] = curCell;
               
            }
        }

        
    }

    int GetSurrondingWallCount(int gridX, int gridY) {
        int wallCount = 0;
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (int neighbourY = gridY -1 ; neighbourY <= gridY + 1; neighbourY++)
            {
                if (neighbourX >= 0 && neighbourX < width && neighbourY >= 0 && neighbourY < height)
                {
                    if (neighbourX != gridX || neighbourY != gridY)
                    {
                        wallCount += map[neighbourX, neighbourY];
                    }
                }
                else {
                    wallCount++;
                }

            }
        }
        return wallCount;
    }


    void DesenharRochas()
    {
        Floodfill.StartMatrix(width, height);
        Tiles = new Tile[width, height];

        if (map != null)
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    SpriteRenderer sprite;
                    Tile stoneObj;
                    if (map[x, y] == 1) 
                    {
                        stoneObj = Instantiate(stone, new Vector3((-width / 2 + x - xOffset) - (-width / 2 + (x - 1) - xOffset) / 6, (-height / 2 + y - yOffset) - (-height / 2 + (y - 1) - yOffset)/6, 0), Quaternion.identity);
                        //sprite = stoneObj.GetComponent<SpriteRenderer>();
                        stoneObj.SetIsWalkable(false);
                        Tiles[x,y] = stoneObj;

                    }
                    else
                    {
                        stoneObj = Instantiate(stone, new Vector3((-width / 2 + x - xOffset)- (-width / 2 + (x-1) - xOffset)/6, (-height / 2 + y - yOffset) - (-height / 2 + (y - 1) - yOffset) / 6, 0), Quaternion.identity);
                        sprite = stoneObj.GetComponent<SpriteRenderer>();
                        var collider = stoneObj.GetComponent<BoxCollider2D>();
                        collider.isTrigger = true;
                        //Destroy(collider);
                        sprite.color = Color.red;
                        stoneObj.SetIsWalkable(true);
                        Tiles[x, y] = stoneObj;
                        //rip aiaiaia// Debug.Log("AIAIAIA");
                    }
                    //xOffset = xOffset - 0.01f;
                    //yOffset = yOffset - 0.01f;
                    stoneObj.InitilizeTile(x, y, double.PositiveInfinity);
                    Floodfill.matriz[x, y] = new cell(x,y, map[x,y], stoneObj, cells[x,y].cost);
                }
            }
        }
    }
}
