using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PathFollower : MonoBehaviour
{
    public float Speed = 1;

    [SerializeField]
    private List<Tile> _points;
    private int _currentTargetIndex;

    private float _distanceToTarget;
    private float _distanceWantsToMoveThisFrame;
    public bool _isFollowingPath;
    public Transform Visuals;
    private DijkstraEnemy _enemy;

    public Tile CurrentTarget
    {
        get
        {
            return _isFollowingPath ? _points[_currentTargetIndex] : null;
        }
    }

    void Awake()
    {
        _currentTargetIndex = 0;
        _isFollowingPath = false;
        _points = new List<Tile>();
        _enemy = GetComponent<DijkstraEnemy>();
    }

    public bool HasReachedTarget()
    {
        return _distanceWantsToMoveThisFrame >= _distanceToTarget;
    }

    public bool IsFollowingPath()
    {
        return _isFollowingPath;
    }

    void MoveCharacter(Vector2 frameMovement)
    {
        Vector2 myPosition = new Vector2(transform.position.x, transform.position.y);
        Visuals.right = frameMovement;
        transform.position = frameMovement + myPosition;
    }

    public void FollowPath(List<Tile> points)
    {
        if (points.Count == 0)
        {
            Debug.LogWarning("Path to follow is empty");
            return;
        }

        _isFollowingPath = true;
        _currentTargetIndex = 0;

        _points.Clear();
        _points.AddRange(points);

        Vector2 myPos2D = new Vector2(transform.position.x, transform.position.y);
        if ((_points[0].WorldPosition - myPos2D).sqrMagnitude < 0.01f)
        {
            _currentTargetIndex++;
            if(_points.Count <= 1)
            {
                _isFollowingPath = false;
            }
        }
    }

    void Update()
    {
        if (!_isFollowingPath) return;

        Vector2 currentTargetPosition = CurrentTarget.WorldPosition;
        Vector2 myPos2D = new Vector2(transform.position.x, transform.position.y);
        Vector2 direction = currentTargetPosition - myPos2D;
        //direction.z = 0;
        _distanceToTarget = direction.magnitude;

        direction.Normalize();

        _distanceWantsToMoveThisFrame = (Speed / (((float)CurrentTarget.PathCost) / 100)) * Time.deltaTime;

        // Faz o movimento terminar exatamente em cima do alvo
        float actualMovementThisFrame = Mathf.Min(_distanceToTarget, _distanceWantsToMoveThisFrame);

        MoveCharacter(actualMovementThisFrame * direction);

        if (HasReachedTarget())
        {
            //_currentTargetIndex = (_currentTargetIndex + 1) % Points.Count;

            _currentTargetIndex++;
            if (_currentTargetIndex == _points.Count)
            {
                _isFollowingPath = false;
            }
        }
    }
}
