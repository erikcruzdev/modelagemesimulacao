﻿using System;
using System.Collections;
using System.Collections.Generic;
using RdPengine;
using UnityEngine;

public class Npc1Controller : MonoBehaviour
{
    public float speed;
    
    public PetriNet npc;
    private Rigidbody2D rb2d;
    
    private float horMov = 0;
    private float verMov = 0;
    private float fator = 0.3F;

    private Place changeDirection;
    
 void Start()
 {
     //if in multi instance, set debug mode explicitally
     //npc = new PetriNet("Assets/enemy.pflow", DebugMode.Exec|DebugMode.Load);
     npc = new PetriNet("Assets/enemy.pflow");
     changeDirection = npc.GetPlaceByLabel("#Time2ChangeDirection");

     rb2d = GetComponent<Rigidbody2D>();
     StartCoroutine("ChangeDirection");
 }
 private void FixedUpdate()
 {
     Vector2 move = new Vector2(horMov, verMov);
     rb2d.AddForce(move * speed);
 }
 IEnumerator ChangeDirection()
 {
     while (true)
     {
         yield return new WaitForSeconds(3);
         changeDirection.Tokens = 1;
         SelectDirection();
     }
 }

 private void SelectDirection()
 {
     switch (npc.GetPlaceByLabel("value").Tokens)
     {
         case 1: horMov = -fator;
             break;
         case 2: horMov = fator;
             break;
         case 3: verMov = fator;
             break;
         case 4: verMov = -fator;
             break;
     }
 }
}
