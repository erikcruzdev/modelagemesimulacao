﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BossRotator : MonoBehaviour
{
    private Vector2 size;
    private float fator;
    private float tamanhoFator;
    private int count, inverte;
    
    
    void Start()
    {
        count = 8;
        inverte = 0;
        fator = 0.0F;
        tamanhoFator = -0.1F;
    }
    void FixedUpdate()
    {
        transform.Rotate( new Vector3(0,0,-280) * Time.deltaTime);
        if (count == 0)
        {
            Vector3 scale = new Vector3( 1.95F + fator, 1.95F + fator, 1f );
            transform.localScale = scale;
            fator += tamanhoFator;
            count = 8;
            inverte++;
            if (inverte == 6)
            {
                tamanhoFator = -tamanhoFator;
                inverte = 0;
            }
        }
        count--;
    }
}
