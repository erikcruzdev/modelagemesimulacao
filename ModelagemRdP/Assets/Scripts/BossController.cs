﻿using RdPengine;
using UnityEngine;

public class BossController : MonoBehaviour
{
    public float speed;

    public PetriNet boss;
    private Rigidbody2D rb2d;

    private Place value;
    
    private float horMov = 3.0F;
    private float verMov = 3.0F;
    private float fator = 1.0F;

    void Start()
    {
        boss = new PetriNet("Assets/boss.pflow");
        value = boss.GetPlaceByLabel("value");
        rb2d = GetComponent<Rigidbody2D>();
    }
    private void FixedUpdate()
    {
        Vector2 movement = new Vector2(horMov, verMov);
        rb2d.AddForce(movement * speed);
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        boss.GetPlaceByLabel("#Collision").Tokens = 1;

        switch (value.Tokens)
        {
            case 1: horMov = -fator;
                break;
            case 2: horMov = fator;
                break;
            case 3: verMov = fator;
                break;
            case 4: verMov = -fator;
                break;
        }
    }
}
