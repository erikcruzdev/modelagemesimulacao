using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RdPengine;

public class Keys : MonoBehaviour
{
    public Text uiKeys, uiWin;
    private string win;
    private int keys;
    private GameObject door;
    private PlayerPetri _playerPetri;
    int maxKeys = 5;

    //Parametrização
    private FloodFill _floodFill;

    // Start is called before the first frame update
    void Start()
    {
        //Parametrização (tem que ajeitar esse .Find)
        _floodFill = GameObject.Find("MapGenerator").GetComponent<FloodFill>();
        maxKeys = _floodFill.NumberOfKeys;
    }

    public void SetPlayerReference(PlayerPetri playerPetri) {
        uiKeys = MainCanvasController.Instance.UIKeys;
        uiWin = MainCanvasController.Instance.UIWin;
        _playerPetri = playerPetri;
        keys = 0;
        uiKeys.text = "Pontos:" + _playerPetri.KeysPlace.Tokens.ToString() + "/" + maxKeys.ToString();
    }


    // Update is called once per frame
    void Update()
    {
       // uiKeys.text = keys.ToString() + "/5";
       if(uiWin!=null)
            uiWin.text = win;
    }

    public void RefreshUI()
    {
        if (_playerPetri != null) {
            Debug.Log("<><><>- CHAMA REFRESH UI!!!!");
            uiKeys.text = "Pontos:" + _playerPetri.KeysPlace.Tokens.ToString() + "/" +maxKeys.ToString();
        }
    }
    public void RefreshWinCondition()
    {
      
        if (_playerPetri.player.GetPlaceByLabel(PlayerPetri.VICTORY).Tokens == 1)
        {
            uiWin.text = "Victory!!!";
        }
        if (_playerPetri.player.GetPlaceByLabel(PlayerPetri.GAME_OVER).Tokens == 1){
            uiWin.text = "Game Over!!!";
            //
        }
        if (_playerPetri.player.GetPlaceByLabel(PlayerPetri.COLLISION_ENEMY).Tokens == 1)
        {
            uiWin.text = "Game Over!!!";
            _playerPetri.gameObject.SetActive(false);
        }
    }

    public void SetTextToUI(string text) {
        uiWin.text = text;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("keys"))
        {
            keys++;
            Destroy(collider.gameObject);
        }
        if (keys==5 && collider.CompareTag("door"))
        {
           // win = "Win!";
        }
    }
}
