using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public static PlayerMovement Instance;
    [SerializeField]
    private float _speed = 15; //Velocidade de Movimento

    [SerializeField]
    private float _maxSpeed = 0; //Velocidade de Movimento

    private Rigidbody2D _rb;

    private PlayerPetri _playerPetri;
    private void Awake()
    {
        Instance = this;
    }
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _playerPetri = GetComponent<PlayerPetri>();
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }

    void Movement()
    {
        Vector2 inputDirection = new Vector2(Input.GetAxisRaw("Horizontal") * _speed + (float)_playerPetri.Stone.Cost, Input.GetAxisRaw("Vertical") * _speed + (float)_playerPetri.Stone.Cost);
        _rb.AddForce(inputDirection);

        //X
        if (_rb.velocity.x > _maxSpeed)
        {
            _rb.velocity = new Vector2(_maxSpeed, _rb.velocity.y);
        }
        else if (_rb.velocity.x < -_maxSpeed)
        {
            _rb.velocity = new Vector2(-_maxSpeed, _rb.velocity.y);
        }

        //Y
        if (_rb.velocity.y > _maxSpeed)
        {
            _rb.velocity = new Vector2(_rb.velocity.x, _maxSpeed);
        }
        else if (_rb.velocity.y < -_maxSpeed)
        {
            _rb.velocity = new Vector2(_rb.velocity.x, _maxSpeed);
        }
    }
}
