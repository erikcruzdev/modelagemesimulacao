using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemineMoveNearPlayer : MonoBehaviour
{

    public Transform target;
    float x;
    float y;
    int r = 0;

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        x = GameObject.FindGameObjectWithTag("Enemy").transform.position.x;
        y = GameObject.FindGameObjectWithTag("Enemy").transform.position.y;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float deltax = Mathf.Abs(x - target.position.x);
        float deltay = Mathf.Abs(y - target.position.y);

        if (Mathf.Sqrt(deltax * deltax + deltay * deltay) < 100)
        {//persegue jogador
            if (target.position.x > x)
            {
                x++;
            }
            else if(target.position.x < x){
                x--;
            }
            if (target.position.y > y) {
                y++;
            }
            else if(target.position.y < y)
            {
                y--;
            }

        }
        else { //anda aleatório

            r = Random.Range(0, 3) % 4;
            switch (r) {
                case 0:
                    x++;
                    break;
                case 1:
                    x--;
                    break;
                case 2:
                    y++;
                    break;
                default:
                    y--;
                    break;
            }
        }
    }
}
