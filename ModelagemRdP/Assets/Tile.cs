using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    [SerializeField]
    private int _posX;

    public int PosX => _posX;

    [SerializeField]
    private int _posY;

    public int PosY => _posY;
    

    public Vector2Int Position { get { return new Vector2Int(_posX, _posY); } }

    public Vector2 WorldPosition { get { return transform.position; } }
    [SerializeField]
    private double _cost;

    public double Cost;

    public float PathCost { get { return (float)Cost / 2 + 50; } }

    [SerializeField]
    private SpriteRenderer _renderer;

    private bool _isWalkable = false;
    public bool IsWalkable => _isWalkable;
    public void SetIsWalkable(bool param) { _isWalkable = param; }
    // Start is called before the first frame update
    void Start()
    {

    }
    public void InitilizeTile(int x, int y, double cost) {
        _posX = x;
        _posY = y;
        _cost = cost;
    
    }

    public void SetCost(double cost) {
        _cost = cost;
    }

    public void SetColor(Color color) {
        _renderer.color = color;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
