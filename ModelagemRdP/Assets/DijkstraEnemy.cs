using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DijkstraEnemy : MonoBehaviour
{
    public Tile InitialPosition;
    public float _timeToChangePath = 5f;
    public float _timer = 5f;
    public List<Tile> Path = new List<Tile>();
    public List<Tile> pathToFollow = new List<Tile>();
    public Tile MyTarget;
    private Tile LastTarget;
    public bool FoundPath;

    public Tile CurrentPos;

    public bool pathReached=false;

    public PathFollower follower;

    public float _timerToMoveToNextTile = 5f;
    public float _timerToMove;

    //Refer�ncia do Player
    private PlayerPetri _player;

    private Tile _currentTileBelowMe;

    private int currentIndex =0;

    ChangePatrolBehaviour _patrol;

    private List<Tile> _patrolPoints = new List<Tile>();

    // Start is called before the first frame update
    void Awake() {

        _patrol = GetComponent<ChangePatrolBehaviour>();
        _patrol.SetDijkstra(this);
        if (_patrol != null) _patrol.FillTargets(_patrolPoints);
    }

    public void FillPatrolList(List<Tile> list) {
        _patrolPoints = list;
        if (_patrol != null) _patrol.FillTargets(list);
    }
    void Start()
    {
        Path = new List<Tile>();
        _currentTileBelowMe = InitialPosition;

        //Achando o player pelo FindWithTag (desculpa Erik kkkkkkk) (nao desculpo >.<)
        _player = PlayerPetri.Instance;
    }
  
    void Update()
    {
        _timer += Time.deltaTime;
        if (_timer >= _timeToChangePath) {
            //if (Input.GetKeyDown(KeyCode.Space)) { 
            Debug.Log(" _patrol.CurrentTarget IS null? " + (_patrol.CurrentTarget == null), _patrol.CurrentTarget);
            MyTarget = _patrol.CurrentTarget;
            if (MyTarget != null)
            {
                Path.Clear();
                FindNewPath();
                _timer = 0;
            }
        }

        if (follower.IsFollowingPath())
        {
            _currentTileBelowMe = follower.CurrentTarget;
        }

        pathReached = !follower.IsFollowingPath();

        if (pathReached) { _patrol.ShouldChangeTarget(); }
       


        //Debug que pega a posi��o do Player
        //Debug.Log("Eu t� te vendo hein, tu ta em: " + _player.tilePosX + " / " + _player.tilePosY);

    }

    public void ChangePathAndTargetmmediate() {
        MyTarget = _patrol.CurrentTarget;
        FindNewPath();
    }

    void FindNewPath() {
        Path.Clear();
        // MyTarget = _player.Stone;
        //if (LastTarget == MyTarget && !_patrol.IsFollowingPlayer) return;
        FoundPath = Dijkstra.Instance.FindPath(_currentTileBelowMe, MyTarget, Path);
        LastTarget = MyTarget;
       
        pathReached = false;

        Debug.Log("LALALALAL");

        follower.FollowPath(Path);

    }

   

}
