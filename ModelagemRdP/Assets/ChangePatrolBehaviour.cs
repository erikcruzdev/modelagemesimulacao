using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RdPengine;

public class ChangePatrolBehaviour : MonoBehaviour
{
    public PetriNet enemy;
    private Place isCloseToPlayer;

    public const string PATROL_STATE = "PatrolState";
    public const string IS_CLOSE_TO_PLAYER = "IsCloseToPlayer";
    public const string FOLLOW_PLAYER = "FollowPlayer";
    public const string PATH = "Assets/inimigo2.pflow";

    Transform _player;
    [SerializeField] private float _radius = 5f;

    [SerializeField] private List<Tile> _patrolPoints = new List<Tile>();
    public List<Tile> PatrolPoints => _patrolPoints;

    [SerializeField] private int currentIndex = 0;
    [SerializeField] public Tile CurrentTarget {  get; private set; }

    public bool IsFollowingPlayer { get; private set; }

    private DijkstraEnemy dijkstraEnemy;

    public bool IsInsidePlayerSight { get; private set; }
    public void SetDijkstra(DijkstraEnemy enemy)
    {
        dijkstraEnemy = enemy;
    }
    // Start is called before the first frame update
    void Start()
    {
        _player = PlayerMovement.Instance.transform;
        IsFollowingPlayer = false;
        CurrentTarget = _patrolPoints[0];

    }
    private Tile MyPos;
    void Awake()
    {
        enemy = new PetriNet(PATH);
        isCloseToPlayer = enemy.GetPlaceByLabel(FOLLOW_PLAYER);
        enemy.GetPlaceByLabel(PATROL_STATE).AddCallback(Patrol, "a", Tokens.In);
        isCloseToPlayer.AddCallback(Follow, "a", Tokens.In);
        
    }

    public void FillTargets(List<Tile> list) {
        _patrolPoints = list;
    }

    void Patrol() {
        // _destination.enabled = false;
        // _patrol.enabled = true;
        currentIndex = (currentIndex + 1) % _patrolPoints.Count;
        CurrentTarget = _patrolPoints[currentIndex];
        Debug.Log("<><><>PATROL!!!!!!!");
        IsFollowingPlayer = false;
        dijkstraEnemy.ChangePathAndTargetmmediate();

    }

    public void ShouldChangeTarget() {
        if (!IsFollowingPlayer)
        {
            currentIndex = (currentIndex + 1) % _patrolPoints.Count;
            CurrentTarget = _patrolPoints[currentIndex];
        }
        else {
            CurrentTarget = PlayerPetri.Instance.Stone;
        }
    }

    void Follow() {
        //_patrol.enabled = false;
        //_destination.enabled = true;
        CurrentTarget = PlayerPetri.Instance.Stone;
        Debug.Log("<><><>FOLLOW!!!!");
        IsFollowingPlayer = true;
        dijkstraEnemy.ChangePathAndTargetmmediate();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 distance = _player.position - transform.position;
        float sqrLen = distance.sqrMagnitude;
        //if (sqrLen < _radius * _radius)
        if (Vector3.Distance(_player.position, transform.position) < _radius)
        {
            enemy.GetPlaceByLabel(PATROL_STATE).Tokens = 0;
            enemy.GetPlaceByLabel(IS_CLOSE_TO_PLAYER).Tokens = 1;
            enemy.GetPlaceByLabel(FOLLOW_PLAYER).Tokens = 1;
            IsInsidePlayerSight = true;
        }
        else {
            enemy.GetPlaceByLabel(PATROL_STATE).Tokens = 1;
            enemy.GetPlaceByLabel(IS_CLOSE_TO_PLAYER).Tokens = 0;
            enemy.GetPlaceByLabel(FOLLOW_PLAYER).Tokens = 0;
            IsInsidePlayerSight = false;
        }
    }
}
